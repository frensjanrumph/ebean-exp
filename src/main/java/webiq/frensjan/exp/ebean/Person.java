package webiq.frensjan.exp.ebean;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Person extends Vertex {

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "person_name", inverseJoinColumns = @JoinColumn(name = "person_name_id"))
    private List<Name> name = new ArrayList<>();

    public List<Name> getName() {
        return name;
    }

    public void setName(List<Name> name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name=" + name +
                "} " + super.toString();
    }
}
