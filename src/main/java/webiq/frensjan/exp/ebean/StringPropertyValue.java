package webiq.frensjan.exp.ebean;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class StringPropertyValue {

    @Id
    public long id;

    public String value;

    public StringPropertyValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "id=" + id +
                ", value='" + value + '\'' +
                '}';
    }
}
