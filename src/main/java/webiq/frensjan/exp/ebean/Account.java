package webiq.frensjan.exp.ebean;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Account extends Vertex {

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "account_name", inverseJoinColumns = @JoinColumn(name = "account_name_id"))
    private List<Name> name = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "account_alias", inverseJoinColumns = @JoinColumn(name = "account_alias_id"))
    private List<Name> alias = new ArrayList<>();

    public List<Name> getName() {
        return name;
    }

    public void setName(List<Name> name) {
        this.name = name;
    }

    public List<Name> getAlias() {
        return alias;
    }

    public void setAlias(List<Name> alias) {
        this.alias = alias;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;
        Account account = (Account) o;
        return Objects.equals(name, account.name) &&
                Objects.equals(alias, account.alias);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, alias);
    }

    @Override
    public String toString() {
        return "Account{" +
                "name=" + name +
                "} " + super.toString();
    }
}
