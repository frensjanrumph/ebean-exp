package webiq.frensjan.exp.ebean;

import io.ebean.Database;
import io.ebean.EbeanServerFactory;
import io.ebean.config.DatabaseConfig;
import io.ebean.datasource.DataSourceConfig;
import org.junit.Test;
import webiq.frensjan.exp.ebean.query.QAccount;
import webiq.frensjan.exp.ebean.query.QPerson;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class EbeanTest {

    @Test
    public void testFieldCollisions() {
        DatabaseConfig config = new DatabaseConfig();
        config.loadFromProperties();
        config.setRegister(true);
        config.setDefaultServer(true);
        config.setDdlGenerate(true);
        config.setDdlRun(true);

        DataSourceConfig dataSourceConfig = new DataSourceConfig()
                .setUsername("sa")
                .setPassword("")
                .setDriver("org.h2.Driver")
                .setUrl("jdbc:h2:mem:");

        config.setDataSourceConfig(dataSourceConfig);

        Database database = EbeanServerFactory.create(config);

        Person person = new Person();
        person.getName().add(new Name("john doe"));

        Account account = new Account();
        account.getName().add(new Name("john doe the second"));
        account.getAlias().add(new Name("johny"));

        database.save(person);
        database.save(account);

        System.out.println("Test load by class and id");

        Person loadedPerson = database.find(person.getClass(), person.getId());
        Account loadedAccount = database.find(account.getClass(), account.getId());

        assertEquals(person, loadedPerson);
        assertEquals(account, loadedAccount);

        System.out.println("Test find by query");

        List<Person> personsNamedJohn = new QPerson()
                .where().name.value.contains("john")
                .findList();

        List<Account> accountsNamedJohn = new QAccount()
                .where().name.value.contains("john")
                .findList();

        assertEquals(person, personsNamedJohn.get(0));
        assertEquals(account, accountsNamedJohn.get(0));

        System.out.println("Test find by root class");

        for (Vertex vertex : database.find(Vertex.class).findList()) {
            if (vertex instanceof Person) {
                assertEquals(person, vertex);
            } else if (vertex instanceof Account) {
                assertEquals(account, vertex);
            } else {
                fail("Unexpected vertex type: " + vertex.getClass());
            }
        }
    }
}
